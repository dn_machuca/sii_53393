#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <errno.h>

#define BUFF_SIZE 128

int main(){
	int fd_fifo;
	char buffer[BUFF_SIZE];
	char nombreFifo[20] = "miFIFO";
	if((mkfifo(nombreFifo, 0777))<0){
		if(errno==EEXIST){}	//Crear FIFO 
		else{
			perror("mkfifo");
			return 1;
		}
	}
	close(0);
	printf("Esperando a que se abra el juego...\n");
	fd_fifo=open(nombreFifo, O_RDONLY);	//Abrirlo en la entrada estándar
	if(fd_fifo <0){
		perror("Open");
		return 2;
	}
	printf("%d", fd_fifo);
	while(1){
		if((read(fd_fifo, buffer, BUFF_SIZE))<=0){
			perror("Read");
			break;
		}
		printf("%s", buffer);
	}

	close(fd_fifo);
	unlink("miFIFO");
	return 0;
}
