
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <stdio.h>
#include <sys/mman.h>

#include "DatosMemCompartida.h"

int main(){
        DatosMemCompartida* shrd;
	char* direccion;

	int fd_comp;

	fd_comp = open("memData", O_RDWR);
	if (fd_comp < 0){
		perror("Open");
		return 1;
	}
	direccion = (char*) mmap(NULL, sizeof(DatosMemCompartida), PROT_READ | PROT_WRITE, MAP_SHARED, fd_comp, 0);
	close(fd_comp);
	shrd = (DatosMemCompartida*) direccion;
	while(1){
		if(shrd->esfera.centro.y > shrd->raqueta1.y1)	shrd->accion = 1;
		else if(shrd->esfera.centro.y < shrd->raqueta1.y2) shrd->accion = -1;
		else shrd->accion = 0;
		usleep(25000);

	}

        return 0;
}

