
// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#pragma once

#include <vector>
#include <string>
#include "DatosMemCompartida.h"
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define BUFF_SIZE 32

#include "Esfera.h"
#include "Raqueta.h"

class CMundo  
{
public:
	void Init();
	CMundo();
	virtual ~CMundo();	

	//Comunicación CLIENTE-SERVIDOR
	void initComunicacion();
	int flagComFIFO; //Flag de comuniación con FIFO 	
	char fifoClienteServidor[20] = "ClienteServidor";
	int crearFIFO ();
	int fdFIFO;	
	//Comunicación de TECLAS
	int fdFIFOTeclas;
	int flagComFIFOTeclas;
	char fifoTeclas[20] = "Teclas";
	void initComunicacionTeclas();
	int crearFIFOTeclas();
	void comunicarFIFOTeclas(char*); //Método de comunicación con el FIFO


	DatosMemCompartida* shrd; //shared
	void crearMemComp();
	int fdMem;

	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	
//	char fifo[20]="miFIFO" ;
	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int puntos1;
	int puntos2;
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
