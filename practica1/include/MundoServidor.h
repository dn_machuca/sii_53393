
// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#pragma once

#include <vector>
#include <string>
#include <pthread.h>
#include "DatosMemCompartida.h"
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define BUFF_SIZE 32

#include "Esfera.h"
#include "Raqueta.h"

class CMundo  
{
public:
	void Init();
	CMundo();
	virtual ~CMundo();	
	void initComunicacion();
	void comunicarFIFO(char*); //Método de comunicación con el FIFO
	int flagComFIFO; //Flag de comuniación con FIFO 	
	int fdFIFO; //Descriptor de fichero del FIFO

	//Comunicación CLIENTE_SERVIDOR
	void initComunicacionCS();
	int fdFIFOCS; //CS: Cliente-Servidor
	int flagComFIFOCS;
	void comunicarFIFOCS(char*);
	char fifoCS[30] = "ClienteServidor";

	//Threads y FIFOTeclas
	pthread_t miThread;
	void RecibeComandosJugador();
	int fdFIFOTeclas;
	char fifoTeclas[20] = "Teclas";
	void initComunicacionTeclas();
	int flagComFIFOTeclas;
/*	DatosMemCompartida* shrd; //shared
	void crearMemComp();
	int fdMem;
*/
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	
	char fifo[20]="miFIFO" ;
	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int puntos1;
	int puntos2;
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
